# `dart`-lukiokurssi

[Hackmd.io](https://hackmd.io/BzhS727vSKa-cZofNzRJ7A)

## Johdanto

Fitech101 Dart-MOOC kurssi koostuu kahdesta osasta:
1. Johdatus ohjelmointiin
   - 1.1. Kirjoittaminen ja lukeminen
   - 1.2. Muistaminen ja laskeminen
   - 1.3. Päätöksenteko
   - 1.4. Toiminnallisuuden tulostaminen
   - 1.5. Ohjelmien pilkkominen pienempiin osiin
   - 1.6. Listalla olevan tiedon käsittely
   - 1.7. Sanakirjassa olevan tiedon käsittely
   - 1.8. Yhteenveto ja opintosuoritus
2. Data ja tieto
   - 2.1. Informaatio, data ja tieto
   - 2.2. Tietotyyppejä ja tiedon esitysmuotoja
   - 2.3. Rakenteellisen tiedon esittäminen luokkien avulla
   - 2.4. Johdatus tietokantoihin
   - 2.5. Tietokantaa käyttävä sovellus, osa 1
   - 2.6. Tiedon hakeminen ja poistaminen
   - 2.7. Tietokantaa käyttävä sovellus, osa 2
   - 2.8. Yhteenveto ja opintosuoritus

Oppitunnit alkavat edellisen viikon tehtävien läpikäynnistä. Seuraavaksi edetään 10min diaesityksellä seuraavan viikon aiheista ja loput ajasta käytetään itsenäisesti materiaalin lukemiseen ja tehtävien tekemiseen.

Oppilaille jää iso osa tehtävien tekemisestä itsenäiseksi työksi.

## Opetussuunnitelma

### Kurssin aikataulu

Kurssi pidetään 3.-4. periodissa tiistain YS-ajalla: `14:45-16:00`. Suuri osa työstä tehdään kotona.

Tavoiteaikataulu on yksi kappale viikossa – yhteenveto ja opintosuoritus jätetään koko kurssin loppuun.

## Kurssin valmistelut

Yleisesti
- Classroom kurssille
    - Hessuun yhteys miten xd
- Kurssille kirjautuminen
- Diat joku tunnille (kesto n. 10min)
    - Minimaalisesti tekstiä (sisällön pitää soveltua formaattiin)
    - Materiaalin runko dioissa
- Kaappo käy ennen kurssia materiaalin läpi ja listaa mitä halutaan joka viikon dioissa kertoa

1. Tunti
   - Diat
       - Oppimistavoitteet
       - Editorin käytöstä screenshotit
       - Kääntäjäkuva
       - Tutustutaan fitech-alustaan:
           - Dartpadin käyttö
           - Monivalintatehtävät
       - Saa tehdä millä editorilla haluaa, mutta kurssin voi suorittaa myös täysin verkossa
       - Kaappo ettii hyvän demon dart/flutter: https://fitech101.aalto.fi/fitech101/mobile-application-development/9-widgets-and-futures/ kurssilta
           - Tarkoitettu inspiraatioksi, ei mennä yksityiskohtiin

2. Tunti

## Kurssiarvostelu

- Kurssista pääsee läpi saamalla 50 % pisteistä
    - Arviointi vaikeaa, suhmuroimalla voi ehkä saada pisteet ylläpidolta (@niklash selvittää)

## Lähteet

- https://fitech101.aalto.fi/fitech101/introduction-to-programming/
- https://fitech101.aalto.fi/fitech101/data-and-information/
